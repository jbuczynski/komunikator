/* jshint node: true */
/* global $: false*/
var app = require("express")();
var httpServer = require("http").Server(app);
var io = require("socket.io")(httpServer);

var static = require('serve-static');
var port = process.env.PORT || 3000;

var oneDay = 86400000;

if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this === null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}

app.use('/img', static(__dirname + '/public/img', { maxAge: oneDay }));
app.use('/js/jquery.min.js', static(__dirname + '/bower_components/jquery/dist/jquery.min.js'));
app.use('/js/jquery.min.map', static(__dirname + '/bower_components/jquery/dist/jquery.min.map'));
app.use(static(__dirname + '/public'));

var usernames = {};
var usersCount = 0;
var userNameNumber = 1;
var chatHistory =[];
var rooms = {};

//var nsp = io.of('/chat');
//nsp.on('connection', function(socket){
io.sockets.on("connection", function (socket) {
    socket.on("message", function (data) {
        var now = new Date();
        chatHistory.push({
                time: now.toTimeString(),
                username: socket.username,
                message: data
        });
        io.sockets.emit("log message", {
                time: now.toTimeString(),
                username: socket.username,
                message: data
            }
        );
    });
   
    socket.on('add user', function () {
        var username = "Anonim" + userNameNumber;
//            + Object.keys(usernames).map(function (element) {
//                var res = element.match(/[^0123456789]+([\d])/);
//                return res[1];
//            }).find(function (element, index, array) {              
//                            
//                if(element !== (index + 1)) {
//                    console.log(element);
//                    return element;
//                } else {
//                     console.log('false');
//                    return 'false';
//                }
//            });
        if (username && !usernames[username]) {
          //to sender socket 
        usersCount++; 
        userNameNumber++;
        var defaultUsername = true;
        socket.username = username;
        usernames[username] = username; 
        socket.emit('login', {
            username: username,
            chatHistory: chatHistory,
            numUsers: usersCount,
            defaultUser: defaultUsername
        });
        var log = {
            time: new Date().toTimeString(),
            username:   "**Chat message**",
            message: username + " dołączył do czatu"
        };
        chatHistory.push(log);
        socket.broadcast.emit('log message', log);
        log.usersCount = usersCount;          
        socket.broadcast.emit('on user count change', log);            
        joinRoom(socket, "Pokój wspólny");          
            
        } else {
            socket.emit('chat login fail', {
                message: "wybierz inną nazwe"
            });
        }
    });
    socket.on("change name", function (username) {
        if (username.toLowerCase() == "Anonim".toLowerCase() || !usernames[username]) {
        usernames[username] = username;
        socket.emit('change name succes', {
            oldName: socket.username,
            newName: username,
        });
        var log = {
            time: new Date().toTimeString(),
            username:   "**Chat message**",
            message: socket.username + " zmienił anzwe na: " + username
        };
        delete usernames[socket.username];
        socket.username = username;

        chatHistory.push(log);

        socket.emit('log message', log); 
        socket.emit('change name success', {name: username});    
        socket.broadcast.emit('log message', log);

    } else {
        socket.emit('chat login fail', {
            message: "wybierz inną nazwe"
        });
    }    
    });
    socket.on("change room", function (name) {
        if(socket.currentRoom !== name) {
            socket.leave(socket.currentRoom);       
                var logMessage = {
                    time: new Date().toTimeString(),
                    username:   "**Chat message**",
                    message: socket.username + "opuścił pokój"
                };
            //leaveRoom(socket);
            socket.broadcast.to(socket.currentRoom).emit('log message', logMessage);
            joinRoom(socket, name);
        }
    });
    socket.on("disconnect", function (err) {
        usersCount--;
         var log = {
            time: new Date().toTimeString(),
            username:   "**Chat message**",
            message: socket.username + " opuścił czat"
        };
        chatHistory.push(log);
        socket.broadcast.emit('log message', log);
        socket.broadcast.emit('on user count change', {usersCount: usersCount});      
        delete usernames[socket.username];
        console.log(Object.keys(usernames));
        socket.broadcast.to(socket.currentRoom).emit('on user leave room',  Object.keys(usernames));
    });
    socket.on("error", function (err) {
        console.dir(err);
    });
    
    var leaveRoom = function(socket) {    
        console.log(usernames);
        //delete usernames[socket.username];
        var usersInRoom =  findClientsSocketInRoom(socket.currentRoom) ;
        socket.leave();
        var new_usernames = [];
        if(usersInRoom && usersInRoom.length >= 1) {
                new_usernames = usersInRoom.map(function(socket) {   
                console.log("w mapie " + socket.username);
                return socket.username;
            });
    }
    console.log("on leave room: " + new_usernames);
    socket.broadcast.to(socket.currentRoom).emit('on user leave room', {name: socket.currentRoom, users: new_usernames});    
    
};

var joinRoom = function(socket, nazwa) {
    socket.join(nazwa);
    var oldName = socket.currentRoom;
    socket.currentRoom = nazwa;
    
    var logMessage = {
        time: new Date().toTimeString(),
        username:   "**Chat message**",
        message: socket.username + ": dołączył do tego pokoju pokoju"
    };
    chatHistory.push(logMessage);
    socket.broadcast.to(nazwa).emit('log message', logMessage);
    //send message to all in specified room
    var usersInRoom =  findClientsSocketInRoom(nazwa) ;
    //console.log(usersInRoom);
    var usernames = [];
   
    if(usersInRoom && usersInRoom.length >= 1) {
            usernames = usersInRoom.map(function(socket) {   
            return socket.username;
        });
    }
    var roomsObj = getAllRooms(rooms);
    var shouldRefreshRooms = roomsObj.roomsChanged;

    if(shouldRefreshRooms) {
        socket.broadcast.emit('on room state change',  roomsObj);
    }
    if(oldName) { socket.broadcast.to(oldName).emit('on user leave room',  findClientsSocketInRoom(oldName).map(function(socket) {   
            return socket.username;})
        );
    }
    socket.emit('join room success', {name: nazwa, users: usernames, rooms: shouldRefreshRooms ? roomsObj.allRooms : null});
    socket.broadcast.to(nazwa).emit('on users state change', {name: nazwa, users: usernames});
};

function findClientsSocketInRoom(roomId) {
    var res = [], room = io.sockets.adapter.rooms[roomId];
    if (room) {
        for (var id in room) {
            res.push(io.sockets.adapter.nsp.connected[id]);
        }
    }
    return res;
}

function getAllRooms() {
        var allRooms = {};
        var sockets = io.of('/').connected;
        // console.log(sockets);
         Object.keys(sockets).map(function(value, index) {
            allRooms[sockets[value].currentRoom] = sockets[value].currentRoom;
            console.log(sockets[value].currentRoom);
         });
        allRooms = Object.keys(allRooms).map(function(value, index) {
            return value;
        });
        var roomsChanged = !compareObjects(rooms, allRooms);

        return {allRooms: allRooms, roomsChanged: roomsChanged};
}

var compareObjects = function(a, b) {
	if (a === b)
		return true;   
	for (var i in a) {
		if (b.hasOwnProperty(i)) {
            console.log(a[i] + "!==" + b[i]);
			if (a[i] !== b[i]) return false;
		} else {
			return false;
		}
	}
 
	for (var j in b) {
		if (!a.hasOwnProperty(j)) {
			return false;
		}
	}
	return true;
};


});

httpServer.listen(port, function () {
    console.log('Serwer HTTP działa na porcie ' + port);
});

