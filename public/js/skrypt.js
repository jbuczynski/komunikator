/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false, $: false*/
"use strict";
// Inicjalizacja
    window.addEventListener("load", function (event) {
        
        var $window = $(window);
        var $usernameInput = $('.usernameInput');
        var $roomnameInput = $('.roomnameInput');
        var $messages = $('.messages');
        var $inputMessage = $('.inputMessage');
        var $loginPage = $('.login.page'); 
        var $changeRoomNamePage = $('.change.roomname.page'); 
        var $chatPage = $('.chat.page');
        var $title = $('.title');
        var status = $("#status");
        var $usersCount = $("#usersCount");
        var $changeNameBtn = $("#changeUsername");
        var $addRoomBtn = $("#addRoom");
        var $roomsPage = $(".rooms.page");
        var socket;
        var roomName;
        
        $usernameInput.on( "keydown", function(event) {
            if(event.which == 13) {     
                setUsername();
            }
        });
        $roomnameInput.on( "keydown", function(event) {
            if(event.which == 13) {     
                addRoom();                
            }
        });
        
        $inputMessage.on( "keydown", function(event) {
            if(event.which == 13) {     
               sendMessage();
            }
        });
        
        $changeNameBtn.click(function() {
            $.blockUI({ message: $loginPage });
        });
        
        $addRoomBtn.click(function() {
            $.blockUI({ message: $changeRoomNamePage });
        });
        
        $(".anulujBtn").click(function() {
            $.unblockUI();
        });
                              

        if (!socket || !socket.connected) {
             //socket = io.connect('/chat');
            //socket = io('/chat');
            socket = io({forceNew: true});
        }
        socket.on('connect', function () {              
            status.attr('src', "img/bullet_green.png");
            console.log('Nawiązano połączenie przez Socket.io');
            //io.of('/chat').socket(id).emit('add user', username);
           socket.emit('add user');
        });
        socket.on('disconnect', function () {             
            status.attr('src', "img/bullet_red.png");
            console.log('Połączenie przez Socket.io zostało zakończone');
        });
        socket.on("error", function (err) {
            $messages.text("Błąd połączenia z serwerem: '" + JSON.stringify(err) + "'");
        });
        socket.on("login", function (data) {
            if(data.chatHistory.length > 0) {
                data.chatHistory.forEach(function (arg) {
                    log(arg.time, arg.username, arg.message);
                });
            }
            if (data.username) {
                if(data.defaultUser) {
                    $title.text('Witaj w czacie' + data.username);
                } else {
                  $.unblockUI({ 
                        onUnblock: function(){ 
                            $title.text('Witaj w czacie ' + data.username);
                            $inputMessage.focus();
                        }
                  });
                }
            }                
        });
        socket.on("log message", function (data) {
            log(data.time, data.username, data.message);
        });        
        socket.on("chat login fail", function (data) {
            console.log("keep username before refreshing");
            $loginPage.find("#message").text(data.message);
            alert();
        });
        socket.on("on user count change", function (data) {
            $usersCount.text(data.usersCount);
        });    
        socket.on("join room success", function (data) {
            alert(JSON.stringify(data));
            
            $roomsPage.find('#roomName').text(data.name);
            var $uListUsernames = $roomsPage.find('.usernames');          
            $uListUsernames.empty();
            
            data.users.forEach(function(user) {
                 var $el = $('<li>').addClass('log').append("<p>" + user + "</p>");
                $uListUsernames.append($el);
            }); 
            if(data.rooms !== null) {
                var $ulListRooms = $roomsPage.find('.rooms');
                $ulListRooms.empty();
                data.rooms.forEach(function(user) {
                     var $el = $('<li>').append("<p>" + user + "</p>");
                    $ulListRooms.append($el);
                });
                $ulListRooms.find('li').click(function() {
                    handleRoomNameClick(this);
                });
            }
        }); 
        socket.on("change name success", function (data) {
            $.unblockUI({ 
                  onUnblock: function(){ 
                      $title.text('Witaj w czacie ' + data.name);
                      $inputMessage.focus();
                  }
            });
        });
        
        socket.on("on user leave room", function (users) {
             alert(JSON.stringify(users));
            var $uListUsernames = $roomsPage.find('.usernames');          
            $uListUsernames.empty();
            users.forEach(function(user) {
                 var $el = $('<li>').addClass('log').append("<p>" + user + "</p>");
                $uListUsernames.append($el);
            }); 
        });
        
        socket.on("on room state change", function (rooms) { 
              var $ulListRooms = $roomsPage.find('.rooms');
                $ulListRooms.empty();
                rooms.allRooms.forEach(function(room) {
                     var $el = $('<li>').append("<p>" + room + "</p>");
                    $ulListRooms.append($el);
                });
                $ulListRooms.find('li').click(function() {
                   handleRoomNameClick(this);
                });
            }
        );
        
        socket.on("on users state change", function (data) {
            alert(JSON.stringify(data));
            $roomsPage.find('#roomName').text(data.name);
            var $uListUsernames = $roomsPage.find('.usernames');          
            $uListUsernames.empty();
            
            data.users.forEach(function(user) {
                 var $el = $('<li>').addClass('log').append("<p>" + user + "</p>");
                $uListUsernames.append($el);
            }); 
        });  
      function addRoom () {
            roomName = cleanInput($roomnameInput.val().trim());
            if(roomName !== "") {
                socket.emit('change room', roomName);
                $.unblockUI();
            } else {
                 $changeRoomNamePage.find("#message").text("Nazwa pokoju musi mieć co najmniej jeden znak");
            }
      }
      function setUsername (username) {
            username = cleanInput($usernameInput.val().trim());
            if(username !== "") {
                socket.emit('change name', username);
            } else {
                 $loginPage.find("#message").text("Nazwa musi mieć co najmniej jeden znak");
            }
      }
     
      function sendMessage () {
        var message = $inputMessage.val();
        console.log('send message');
        message = cleanInput(message);
        if (message && socket.connected) {
          $inputMessage.val('');
          socket.emit('message', message);
        }
      }

      function cleanInput (input) {
        console.log('clean input');
        return $('<div/>').text(input).text();
      }

      function log (time, username, message) {
        var $el = $('<li>').addClass('log').append("<p class='time'>" + time + "</p><p class='username'>" + username + ":</p><p class='message'> " + message + "</p>" );
        $messages.append($el);
        $chatPage.scrollTop($chatPage.scrollTop() + $el.position().top);
      }
      function handleRoomNameClick (elem) {
           socket.emit('change room', $(elem).text());
      }
});